#include "stdafx.h"
#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/thread/scoped_thread.hpp>
#include <boost/exception/exception.hpp>
#include <fstream>
#include <string.h>

namespace fs = boost::filesystem;
using boost::multiprecision::cpp_int;
cpp_int sum = 0;

void wait(int seconds) {
	boost::this_thread::sleep_for(boost::chrono::seconds{ seconds });
}

bool isValidInteger(char ch[]) {
	for (size_t i = 0; i < strlen(ch); ++i){
		if (ch[i] < '0' || ch[i] > '9')
			return false;
	}
	return true;
}

void threadLoadFile(fs::path pathFile) {
	cpp_int val = 0;
	std::string fileName = pathFile.filename().string();
	std::ifstream fin(pathFile.string(), std::ios_base::in);
	if (!fin.is_open()) {
		std::cerr << "Could not open file " + fileName + " from folder\n";
		fin.clear();
	}
	try {
		char ch[84];
		fin.getline(ch, 84);
		if (!isValidInteger(ch)){
			std::cout << "Incorrect data in file\n";
		}
		val = boost::lexical_cast<cpp_int>(ch);
		std::cout << fileName + ": " << val << std::endl;
		sum = sum + val;
		wait(1);
	}
	catch (std::ifstream::failure e){
		std::cout << "Exception reading file" << std::endl;
	}
	fin.close();
}

int _tmain(int argc, _TCHAR* argv[])
{
	//setlocale(LC_ALL, "Russian");
	std::string pathStr;
	std::cout << "Please enter path to the directory: \n";
	//"C:\path\to\your\directory\"/
	getline(std::cin, pathStr);
	fs::path pathDir(pathStr);
	fs::directory_iterator end_iter;
	cpp_int val = 0;
	if (fs::exists(pathDir) && fs::is_directory(pathDir)) {
		for (fs::directory_iterator dir_iter(pathDir); dir_iter != end_iter; ++dir_iter) {
			boost::scoped_thread<> t(&threadLoadFile, dir_iter->path());
		}
		std::cout << "Sum of all numbers from files in folder: " << sum << std::endl;
	}
	return 0;
}