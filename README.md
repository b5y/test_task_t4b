This is a testing task for position "C++ programmer" from the Russian company.
<p>Statement of a problem:</p>
You have a directory on computer with many files and you don't know exactly
every file's name. Each file contains a number and you should check it for
correctness. There needs to implement win32 console application which
takes directory path as input and processes with files in the following way:
<p>Every file needs to be executed in this order:</p>
<ol>
  <li>Read number from file</li>
  <li>After reading and receiving number one should output its value and file
  name to stdout. For instance:
    <p>hello.txt: 3</p>
    <p>test1: 1</p>
    <p>test2: 7</p></li>
  <li>Later process needs to pause current stream for 1 second.</li>
</ol>
<p>Subsequently at the end of the programm user should see sum of all numbers.</p>
<p>Additional statements:</p>
<ul>
  <li>program should be implemented in Microsoft Visual Studio (2010 or 2012, Express edition permitted)</li>
  <li>as advantage is the use of Boost library</li>
  <li>program should process files concurrently</li>
</ul>

<p>I implemented this program using boost version 1.58 and Visual Studio 2013.</p>
<p>Any comments and suggestions will be highly appreciated.</p>
